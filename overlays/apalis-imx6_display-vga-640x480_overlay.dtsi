// SPDX-License-Identifier: GPL-2.0-or-later OR MIT
/*
 * Copyright 2022 Toradex
 */

// VGA Signal 640x480@60Hz Industry standard timing, display-enable
// polarity set as required by the resistor ladder video DAC on the
// Apalis iMX6.

&{/vga-panel} {
	compatible = "panel-dpi";
	status = "okay";

	/* for 0.3mm pixels */
	width-mm = <192>;
	height-mm = <144>;

	panel-timing {
		clock-frequency = <25175000>;
		hactive = <640>;
		vactive = <480>;
		hsync-len = <96>;
		hfront-porch = <16>;
		hback-porch = <48>;
		vsync-len = <2>;
		vfront-porch = <10>;
		vback-porch = <33>;

		de-active = <0>;
		hsync-active = <0>;
		vsync-active = <0>;
		pixelclk-active = <0>;
	 };
};
