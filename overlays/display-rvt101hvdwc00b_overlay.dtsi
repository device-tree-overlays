// SPDX-License-Identifier: GPL-2.0-or-later OR MIT
/*
 * Copyright 2023 Toradex
 */

// RVT101HVDWC00-B DSI display (10 inch) with a resolution of 1280x800 pixel.

&panel_lvds {
	compatible = "panel-lvds";
	backlight = <&backlight>;
	data-mapping = "vesa-24";
	height-mm = <136>;
	width-mm = <217>;
	status = "okay";

	/* 
	 * These timings are hand-optimized and more stable then teorethical
	 * In particular HBP+HS should be 88 (display datasheet)
	 * Likely an higher value is needed due to dsi to lvds pipeline.
	 */
	panel-timing {
		clock-frequency = <66300000 69300000 78900000>;
		de-active = <1>;
		hactive = <1280 1280 1280>;
		hback-porch = <64 64 64>;
		hfront-porch = <72 72 72>;
		hsync-active = <0>;
		hsync-len = <32 32 32>;
		pixelclk-active = <1>;
		vactive = <800 800 800>;
		vback-porch = <15 15 15>;
		vfront-porch = <15 15 15>;
		vsync-active = <0>;
		vsync-len = <8 8 8>;
	};

	port {
		panel_in_lvds_riv: endpoint {
			remote-endpoint = <&lvds_out_panel_riv>;
		};
	};
};
