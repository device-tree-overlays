// SPDX-License-Identifier: GPL-2.0-or-later OR MIT
/*
 * Copyright 2022 Toradex
 */

// Verdin DSI to LVDS Adapter with connected LT170410 display (10 inch) with a
// resolution of 1280x800 pixel. Adapter and display can be ordered at Toradex.

&lvds_ti_sn65dsi84 {
	status = "okay";

	ports {
		#address-cells = <1>;
		#size-cells = <0>;

		port@2 {
			reg = <2>;

			lvds_out_panel: endpoint {
				remote-endpoint = <&panel_in_lvds>;
			};
		};
	};
};

&panel_lvds {
	compatible = "panel-lvds";
	backlight = <&backlight>;
	data-mapping = "vesa-24";
	height-mm = <136>;
	power-supply = <&reg_3p3v>;
	width-mm = <217>;
	status = "okay";

	panel-timing {
		clock-frequency = <68900000 71100000 73400000>;
		de-active = <1>;
		hactive = <1280 1280 1280>;
		hback-porch = <23 60 71>;
		hfront-porch = <23 60 71>;
		hsync-len = <15 40 47>;
		pixelclk-active = <1>; /* positive edge */
		vactive = <800 800 800>;
		vback-porch = <5 7 10>;
		vfront-porch = <5 7 10>;
		vsync-len = <6 9 12>;
	};

	port {
		panel_in_lvds: endpoint {
			remote-endpoint = <&lvds_out_panel>;
		};
	};
};
