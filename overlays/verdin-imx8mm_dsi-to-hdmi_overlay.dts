// SPDX-License-Identifier: GPL-2.0-or-later OR MIT
/*
 * Copyright 2022 Toradex
 */

// Verdin DSI to HDMI Adapter orderable at Toradex.

/dts-v1/;
/plugin/;

#include <dt-bindings/gpio/gpio.h>

/ {
	compatible = "toradex,verdin-imx8mm";
};

&{/} {
	reg_dsi_hdmi: regulator-dsi-hdmi {
		compatible = "regulator-fixed";
		enable-active-high;
		/* Verdin CTRL_SLEEP_MOCI# (SODIMM 256) */
		gpio = <&gpio5 1 GPIO_ACTIVE_HIGH>;
		regulator-boot-on;
		regulator-name = "DSI_1_PWR_EN";
	};
};

&mipi_dsi {
	samsung,esc-clock-frequency = <16000000>;
	status = "okay";

	ports {
		#address-cells = <1>;
		#size-cells = <0>;

		port@1 {
			reg = <1>;

			mipi_dsi_bridge1_out: endpoint {
				attach-bridge;
				remote-endpoint = <&lt8912_1_in>;
			};
		};
	};
};

&hdmi_lontium_lt8912 {
	vdd-supply = <&reg_dsi_hdmi>;
	status = "okay";

	ports {
		#address-cells = <1>;
		#size-cells = <0>;

		port@0 {
			reg = <0>;

			lt8912_1_in: endpoint {
				data-lanes = <1 2 3 4>;
				remote-endpoint = <&mipi_dsi_bridge1_out>;
			};
		};

		port@1 {
			reg = <1>;

			lt8912_1_out: endpoint {
				remote-endpoint = <&hdmi_connector_in>;
			};
		};
	};
};

&hdmi_connector {
	status = "okay";

	port {
		hdmi_connector_in: endpoint {
			remote-endpoint = <&lt8912_1_out>;
		};
	};
};

/* Verdin I2C_2_DSI */
&i2c2 {
	/* Lower frequency to avoid DDC/EDID issues with certain displays/screens. */
	clock-frequency = <10000>;
	status = "okay";
};

&lcdif {
	status = "okay";
};

&pwm1 {
	status = "disabled";
};
