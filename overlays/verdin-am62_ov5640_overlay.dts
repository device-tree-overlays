// SPDX-License-Identifier: GPL-2.0-or-later OR MIT
/*
 * Copyright 2023 Toradex
 */

// CSI Camera Module 5MP OV5640

/dts-v1/;
/plugin/;

#include <dt-bindings/gpio/gpio.h>

/ {
	compatible = "toradex,verdin-am62";
};

&{/} {
	clk_ov5640_osc: ov5640-xclk {
		compatible = "fixed-clock";
		#clock-cells = <0>;
		clock-frequency = <24000000>;
	};

	regulator_camera: regulator-camera {
		compatible = "regulator-fixed";
		/* Verdin GPIO_8_CSI - Camera Connector 24 */
		gpio = <&main_gpio0 42 GPIO_ACTIVE_HIGH>;
		enable-active-high;
		regulator-name = "V_CSI";
		startup-delay-us = <5000>;
	};
};

&main_i2c3 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	ov5640_csi_cam: camera@3c {
		compatible = "ovti,ov5640";
		reg = <0x3c>;
		clocks = <&clk_ov5640_osc>;
		clock-names = "xclk";
		AVDD-supply = <&regulator_camera>;
		DVDD-supply = <&regulator_camera>;
		DOVDD-supply = <&regulator_camera>;
		/* Verdin GPIO6 - Camera Connector 22 */
		powerdown-gpios = <&main_gpio0 36 GPIO_ACTIVE_HIGH>;
		/* Verdin GPIO5 - Camera Connector 11 */
		reset-gpios = <&main_gpio0 40 GPIO_ACTIVE_LOW>;

		port {
			csi2_cam0: endpoint {
				remote-endpoint = <&csi2rx0_in_sensor>;
				clock-lanes = <0>;
				data-lanes = <1 2>;
			};
		};
	};
};

&csi0_port0 {
	status = "okay";

	csi2rx0_in_sensor: endpoint {
		remote-endpoint = <&csi2_cam0>;
		bus-type = <4>; /* CSI2 DPHY. */
		clock-lanes = <0>;
		data-lanes = <1 2>;
	};
};

&dphy0 {
	status = "okay";
};

&ti_csi2rx0 {
	status = "okay";
};
