// SPDX-License-Identifier: GPL-2.0-or-later OR MIT
/*
 * Copyright 2022 Toradex
 */

// Parallel RGB controller as used by the resistor ladder video DAC on
// the Apalis iMX6.

&{/} {
	vga_display: disp1 {
		compatible = "fsl,imx-parallel-display";
		#address-cells = <1>;
		#size-cells = <0>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_ipu2_vdac>;
		status = "okay";

		port@0 {
			reg = <0>;

			vga_display_in: endpoint {
				remote-endpoint = <&ipu2_di0_disp0>;
			};
		};

		port@1 {
			reg = <1>;

			vga_display_out: endpoint {
				remote-endpoint = <&vga_bridge_in>;
			};
		};
	};

	vga-panel {
		data-mapping = "rgb565";

		port {
			vga_bridge_in: endpoint {
				remote-endpoint = <&vga_display_out>;
			};
		};
	};
};

&ipu2 {
	#address-cells = <1>;
	#size-cells = <0>;

	port@2 {
		ipu2_di0_disp0: endpoint@0 {
			remote-endpoint = <&vga_display_in>;
		};
	};
};
